def reverser
  words = yield.split
  reversed_words = words.map { |i| reversed_word(i) }
  reversed_words.join(' ')
end

def reversed_word(word)
  new_word = []
  word.each_char do |ch|
    new_word.unshift(ch)
  end
  new_word.join
end

def adder(n = 1)
  n + yield
end

def repeater(n = 1)
  n.times { yield }
end
